package com.general.task;

import java.util.Scanner;

public class Main {

    static int solveQuadraticEquation(Scanner in) {
        double[] par = new double[3];
        System.out.println("Для уравнения ax^2+bx+c=-7, задайте параметры a, b, c");
        try {
            par[0] = in.nextDouble();
            par[1] = in.nextDouble();
            par[2] = in.nextDouble();
        } catch (Exception ex) {
            System.out.println("Неправильный ввод, необходимо ввести число");
            return -1;
        }
        QuadraticEquation qe = new QuadraticEquation(par);
        double[] d = qe.calculate();
        if (d[0] == 0 && d[1] == 0)
            System.out.format("Не имеет корней\n");
        else
            System.out.format("x1 = %.2f; x2 = %.2f\n", d[0], d[1]);
        return 0;
    }

    static int solveArrayOfFifteen(Scanner in) {
        int cardinality = 15;
        int broad = 15;
        int count = 0;
        int num = 0;
        System.out.format("Введите числа, значения которых, больше %d и количество элементов равное %d \n", broad, cardinality);
        int ar[] = new int[cardinality];
        try {
            while (count < cardinality) {
                num = in.nextInt();
                if(num < 16) {
                    System.out.println("Число должно быть больше 15");
                    continue;
                }
                ar[count] = num;
                count++;
            }
        }
        catch (Exception ex) {
            System.out.println("Неправильный ввод, необходимо ввести число");
            return -1;
        }
        ArrayOfFifteen af = new ArrayOfFifteen(ar, broad);
        try {
            af.fillArray();
            af.show();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return -1;
        }
        return 0;
    }

    static int solveMagic(Scanner in) {
        int N;
        System.out.println("Введите порядок магисечкого квадрата больше 2");
        try {
            N = in.nextInt();
        } catch (Exception ex) {
            System.out.println("Неправильный ввод, необходимо ввести число");
            return -1;
        }
        if (N < 3) {
            System.out.println("Неправильный ввод, число должно быть больше 2");
            return -1;
        } else {
            MagicSquare magicSquare = new MagicSquare(N);
            magicSquare.solveMagic();
            magicSquare.show();
        }
        return 0;
    }

    public static void main(String[] args) {
        int status = 0;
        byte jobNumber = 3;
        Scanner in = new Scanner(System.in);
        System.out.format("Задания: \n 1. Квадратное уравнение \n 2. Заполнение массива \n 3. Магический квадрат \n");
        System.out.println("Введите номер задания (по умолчанию - 1)");
        if (in.hasNextByte())
            jobNumber = in.nextByte();
        if (jobNumber > 3 || jobNumber < 1) {
            System.out.println("Неверный ввод числа");
            status = -1;
        } else {
            switch (jobNumber) {
                case 1:
                    status = solveQuadraticEquation(in);
                    break;
                case 2:
                    status = solveArrayOfFifteen(in);
                    break;
                case 3:
                    status = solveMagic(in);
                    break;
            }
        }
        System.exit(status);
    }
}
