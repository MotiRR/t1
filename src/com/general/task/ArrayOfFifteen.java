package com.general.task;

import java.util.Arrays;

/* Дан пустой массив из 15 элементов. На вход подаются 15 различных чисел, каждое из которых больше 15.
Нужно заполнить массив по следующему алгоритму: взять значение входного числа по модулю 15 и положить в ячейку
массива с соответствующим номером. В случае, если ячейка занята, положить в ячейку с номером i + 3 (где i –
изначально получившийся номер ячейки). Так до тех пор, пока не удастся положить число в массив. В случае
зацикливания кинуть исключение.*/
public class ArrayOfFifteen {
    int ar[];
    int broad;

    ArrayOfFifteen(int ar[], int broad) {
        this.ar = ar;
        this.broad = broad;
    }

    public void fillArray() throws Exception {
        int n = ar.length - 1;
        int generalIterations = ar.length/3;
        int a[] = new int[n+1];
        for (int i = 0; i <= n; i++) {
            int ost = ar[i] % broad;
            if (a[ost] < broad) a[ost] = ar[i];
            else {
                int j = ost, iter = 0;
                while (iter < generalIterations) {
                    j = j + 3;
                    if(j > n) {j = j - n;}
                    if (a[j] < broad) {a[j] = ar[i]; break;}
                    if(++iter == generalIterations) {
                        throw new Exception("Exception: loop");
                    }

                }
            }
        }
    }

    public void show() {
        System.out.println(Arrays.toString(ar));
    }
}
