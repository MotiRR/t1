package com.general.task;

import org.omg.CORBA.IntHolder;

public class MagicSquare {

    int n;
    int m[][];

    public MagicSquare(int N) {
        this.n = N;
        this.m = new int[n][n];
    }

    private void fillMatrix() {
        int sum = 0;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                m[i][j] = ++sum;
            }
    }

    private void fillTempMatrixOnePartOne(int[][] tempMatrixOne, int i, IntHolder l, int m0)
    {
        boolean go = true;
        for (int k = 1; ; k++) {
            if (i >= m0 / 2 && go) {
                l.value = l.value - 1;
                go = false;
            }
            if (k <= l.value) {
                tempMatrixOne[i + k][i] = 1;
            } else break;
        }
    }
    private void fillTempMatrixOnePartTwo(int[][] tempMatrixOne, int i, IntHolder h, int m0)
    {
        boolean went = true;
        for (int k = m0 / 2; ; k++) {
            if (i > 0 && went) {
                h.value = h.value - 1;
                went = false;
            }
            if (k <= h.value) {
                tempMatrixOne[i][i + k] = 1;
            } else break;
        }
    }

    private int[][] fillTempMatrixOne(int m0) {
        IntHolder l = new IntHolder(m0 / 2);
        IntHolder h = new IntHolder(m0 - 1);
        int[][] tempMatrixOne = new int[m0][m0];
        for (int i = 0; i < m0; i++) {
            fillTempMatrixOnePartOne(tempMatrixOne, i, l, m0);
            fillTempMatrixOnePartTwo(tempMatrixOne, i, h, m0);
        }
        return tempMatrixOne;
    }

    private int[][] fillTempMatrixTwo(int[][] tempMatrixOne) {
        int n = tempMatrixOne.length;
        int[][] tempMatrixTwo = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                tempMatrixTwo[i][j] = tempMatrixOne[i][n - j - 1];
            }
        }
        return tempMatrixTwo;
    }

    private void fillTempEvenMatrixOnePartOne(int[][] tempEvenMatrixOne, int i, IntHolder l) {
        boolean go = true;
        for (int k = 1; ; k++) {
            if (i >= 3 && go) {
                l.value = l.value - 1;
                go = false;
            }
            if (k <= l.value) {
                tempEvenMatrixOne[i + k][i] = 1;
            } else break;
        }
    }

    private void fillTempEvenMatrixOnePartTwo(int[][] tempEvenMatrixOne, int i, IntHolder h, int m0) {
        boolean went = true;
        for (int k = 0; ; k++) {
            if (i > 0 && went) {
                h.value = h.value - 1;
                went = false;
            }
            if (k < h.value) {
                tempEvenMatrixOne[i][m0 - k - 1] = 1;
            } else break;
        }
    }

    private int[][] fillTempEvenMatrixOne(int m0) {
        IntHolder l = new IntHolder((m0 - 1) / 2);
        IntHolder h = new IntHolder((m0 - 1) / 2);
        int[][] tempEvenMatrixOne = new int[m0][m0];
        tempEvenMatrixOne[m0 - 1][0] = 2;
        tempEvenMatrixOne[m0 - 1][1] = 3;
        tempEvenMatrixOne[m0 - 2][0] = 3;
        for (int i = 0; i < m0 - 1; i++) {
            tempEvenMatrixOne[i][i + 1] = 2;
        }
        for (int i = 0; i < m0 - 2; i++) {
            tempEvenMatrixOne[i][i + 2] = 3;
        }
        if (n == 6) {
            for (int i = 0; i < m0; i++) {
                tempEvenMatrixOne[i][i] = 1;
            }
        } else {
            for (int i = 0; i < m0; i++) {
                fillTempEvenMatrixOnePartOne(tempEvenMatrixOne, i, l);
                fillTempEvenMatrixOnePartTwo(tempEvenMatrixOne, i, h, m0);
            }
        }
        return tempEvenMatrixOne;
    }

    private void evenOrderPartTwo(int middle) {
        m[0][0] = m[middle][middle] + n;
        m[n - 1][n - 1] = m[middle][middle] - n;
        m[n - 1][0] = m[middle][middle] + 1;
        m[0][n - 1] = m[middle][middle] - 1;
    }

    private void evenOrderPartThree(int middle) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (m[i][j] == 0) {
                    if (i < middle) {
                        if ((j == 0 && i != 0) || (j == n - 1 && i != 0))
                            m[i][j] = m[i - 1][j] + (n - 1);
                    }
                    if (i > middle) {
                        if (j == 0 || j == n - 1)
                            m[i][j] = m[n - 1][j] - (n - 1) % i * (n - 1);
                    }

                }
            }
        }
    }

    private void evenOrderPartFour(int middle) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (m[i][j] == 0) {
                    if (j < middle && j != 0) {
                        m[i][j] = m[i][j - 1] + (n + 1);
                    }
                    if (j > middle) {
                        m[i][j] = m[i][n - 1] - (n - 1) % j * (n + 1);
                    }
                }

            }
        }
    }

    private void evenOrder() {
        int start = n;
        int middle = (n + 1) / 2 - 1;
        for (int i = 0; i < n; i++) {
            if (i <= middle) {
                m[i][middle - i] = start;
                start -= 2;
                if (i > 0) {
                    for (int j = middle - i + 1; j <= middle + i; j++) {
                        m[i][j] = m[i][j - 1] + n + 1;
                    }
                }
            } else {
                m[i][i - middle] = (m[i - 1][i - middle - 1] + 2 * n);
                for (int j = i - middle + 1; j <= (n - (i - middle) - 1); j++) {
                    m[i][j] = m[i][j - 1] + n + 1;
                }
            }
        }
        evenOrderPartTwo(middle);
        evenOrderPartThree(middle);
        evenOrderPartFour(middle);
    }

    private void oddEvenOrderPartOne(int[][] tempEvenMatrixOne, int i, int j) {
        int swap = 0;
        if (tempEvenMatrixOne[i][j] == 1) {
            swap = m[i][j];
            m[i][j] = m[n - i - 1][n - j - 1];
            m[n - i - 1][n - j - 1] = swap;
        }
    }

    private void oddEvenOrderPartTwo(int[][] tempEvenMatrixOne, int i, int j) {
        int swap = 0;
        if (tempEvenMatrixOne[i][j] == 2) {
            if (j == 0) {
                swap = m[i][j];
                m[i][j] = m[i + 1][j];
                m[i + 1][j] = swap;
            } else {
                swap = m[i][j];
                m[i][j] = m[n - i - 1][j];
                m[n - i - 1][j] = swap;
            }
        }
    }

    private void oddEvenOrderPartThree(int[][] tempEvenMatrixOne, int i, int j) {
        int swap = 0;
        if (tempEvenMatrixOne[i][j] == 3) {
            swap = m[i][j];
            m[i][j] = m[i][n - j - 1];
            m[i][n - j - 1] = swap;
        }
    }

    private void oddEvenOrderPartFour(int[][] tempEvenMatrixTwo, int i, int j, int m0) {
        int swap = 0;
        if (tempEvenMatrixTwo[i][j % m0] == 1) {
            swap = m[i][j];
            m[i][j] = m[n - i - 1][n - j - 1];
            m[n - i - 1][n - j - 1] = swap;
        }
    }

    private void oddEvenOrder() {
        int m0 = n / 2;
        fillMatrix();
        int[][] tempEvenMatrixOne = fillTempEvenMatrixOne(m0);
        int[][] tempEvenMatrixTwo = fillTempMatrixTwo(tempEvenMatrixOne);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                if (i < n / 2 && j < n / 2) {
                    oddEvenOrderPartOne(tempEvenMatrixOne, i, j);
                    oddEvenOrderPartTwo(tempEvenMatrixOne, i, j);
                    oddEvenOrderPartThree(tempEvenMatrixOne, i, j);
                }
                if (i < n / 2 && j >= n / 2) {
                    oddEvenOrderPartFour(tempEvenMatrixTwo, i, j, m0);
                }
            }
    }

    private void oddOrderPartOne(int[][] tempMatrixOne, int i, int j) {
        int swap = 0;
        if (i < n / 2 && j < n / 2) {
            if (tempMatrixOne[i][j] == 1) {
                swap = m[i][j];
                m[i][j] = m[n - i - 1][n - j - 1];
                m[n - i - 1][n - j - 1] = swap;
            }
        }
    }

    private void oddOrderPartTwo(int[][] tempMatrixTwo, int i, int j, int m0) {
        int swap = 0;
        if (i < n / 2 && j >= n / 2) {
            if (tempMatrixTwo[i][j % m0] == 1) {
                swap = m[i][j];
                m[i][j] = m[n - i - 1][n - j - 1];
                m[n - i - 1][n - j - 1] = swap;
            }

        }
    }

    private void oddOrder() {

        int m0 = n / 2;
        fillMatrix();
        int[][] tempMatrixOne = fillTempMatrixOne(m0);
        int[][] tempMatrixTwo = fillTempMatrixTwo(tempMatrixOne);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                oddOrderPartOne(tempMatrixOne, i, j);
                oddOrderPartTwo(tempMatrixTwo, i, j, m0);
            }
    }

    public void solveMagic() {
        if (n % 2 > 0)
            evenOrder();
        else if ((n / 2) % 2 == 0) oddOrder();
        else oddEvenOrder();
    }

    public void show() {
        System.out.println("Квадратичная матрица:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(m[i][j] + "\t");
            }
            System.out.println();
        }
    }

}
