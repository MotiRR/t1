package com.general.task;

/*Решить квадратное уравнение ax^2+bx+c=-7. На вход в программу подаются значения a,b,c. На выходе хотим
    получить решение уравнения
     */
class QuadraticEquation {

    private double a, b, c;
    private double[] answer;

    public QuadraticEquation(double[] par) {
        this.answer = new double[]{0, 0};
        this.a = par[0];
        this.b = par[1];
        this.c = par[2];
    }

    public double[] calculate() {
        if (a == 0 && b == 0 && c == 0) return answer;
        if (a != 0 && b != 0 && c != 0) {
            return calculateRootsStandard();
        } else return calculateSpecial();
    }

    private double[] calculateRootsStandard() {
        double D = Math.pow(b, 2) - 4 * a * (c + 7);
        if (D > 0) {
            answer[0] = ((-b) + Math.sqrt(D)) / (2 * a);
            answer[1] = ((-b) - Math.sqrt(D)) / (2 * a);
        } else if (D == 0) {
            answer[0] = -b / (2 * a);
        }
        return answer;
    }

    private double[] calculateSpecial() {

        if (a == 0) {
            answer[0] = (-7 - c) / b;
        }
        if (b == 0 && c == 0) return answer;
        if (b == 0) {
            double roof = (-7 - c) / a;
            if (roof < 0) return answer;
            else {
                answer[0] = Math.sqrt(roof);
                answer[1] = -Math.sqrt((roof));
            }
        }
        if (c == 0) {
            answer[1] = -b / a;
        }
        return answer;
    }

}

