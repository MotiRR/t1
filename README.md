# Задачи

1. Реализовать решение квадратного уравнения.
2. Определение места числа в массиве.
3. Создать магический квадрат порядка N.

# Инструменты
+ InteliJ IDEA
+ JDK 1.8

# Установка
Открываем командную строку в загруженной папке и в командной строке пишем
```
mkdir bin
javac -sourcepath ./src/ -d bin src/com/general/task/Main.java
```
# Запуск
```
java -classpath ./bin com/general/task/Main
```

